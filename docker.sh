#!/bin/bash

echo "🏗 Generate the secret key for the app"
export SENTRY_SECRET_KEY="$(docker run --rm sentry config generate-secret-key)"

echo "🐳 Create the containers"
docker-compose up -d

echo "⬆️ Update sentry and create the migrations"
docker-compose exec sentry-server sentry upgrade

echo "♻️ Restart sentry"
docker-compose restart sentry-server
